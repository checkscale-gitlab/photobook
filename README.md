# PhotoBook

Continuation of [https://git.transistories.org/pcassima/photobook-old](https://git.transistories.org/pcassima/photobook-old)

[www.pcassima.net](https://www.pcassima.net "my personal photography portfolio")

All source code for my personal photography portfolio. This project was started as a way to learn the P.E.R.N. stack.

This project uses:

- Typescript
- Postgress
- Express.js
- React
- Node.js
- Docker
